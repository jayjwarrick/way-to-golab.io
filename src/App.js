import React from 'react';
import {HashRouter, Redirect, Route, Link} from 'react-router-dom';
import {concat, first, includes} from 'lodash';

import content from './content';
import categorizedPages from './categorizedPages';
import languages from './languages';
import Lesson from './components/Lesson';

import './App.css';
import './menu.css';

function LanguageSelect({page, locale}) {
    return <div id="lang-select">
        {languages.map(
            language =>
                <div key={locale} className="lang-option">
                    <Link to={`/${language.locale}/${page}`}>
                    {language.name}
                    </Link>
                </div>
        )}
    </div>
}

function TopLevelNav({categories, page, locale}) {
    if (!categories) {
        return null;
    }

    return <ul id="tl-nav">
        {categorizedPages.map(
            category =>
                <li key={category.name} className={includes(category.pages, page)?'active-tl':''}>
                    <Link to={`/${locale}/${first(category.pages)}`}>
                        {categories[category.name]}
                    </Link>
                </li>
        )}
    </ul>;
}

function NavSection(category, index, localizedCategories, currentPage, locale, navClick) {
    if (!localizedCategories) {
        return;
    }

    const oneBasedIndex = index + 1;

    const subNav = category.pages.map(
        page => {
            const current = page === currentPage ? 'current-sub' : '';
            return <Link to={`/${locale}/${page}`} className={`${current} sub-nav`} key={page} onClick={navClick}>
                <div className="sub-nav-title">{localizedCategories[page].title}</div>
                <div className="sub-nav-caption">{localizedCategories[page].caption}</div>
            </Link>;
        }
    );
    const isActive = includes(category.pages, currentPage);
    const activeNH = isActive ? 'active-nh' : '';
    const activeSub = isActive ? 'active-sub' : '';
    return [
        <label key={category.name} className={"nav-header " + activeNH} htmlFor={`nav-${oneBasedIndex}`}>
            {localizedCategories[category.name]}
        </label>,
        <input id={`nav-${oneBasedIndex}`} className="nav-tgl" type="radio" name="nav" key={`nav-${oneBasedIndex}`}/>,
        <div className={"sub-menu " + activeSub} key={`sub-menu-${oneBasedIndex}`}>
            <label htmlFor="nav-0" className="sub-nav">
                <i className="fas fa-arrow-left"></i> {localizedCategories[category.name]}
            </label>
            {subNav}
        </div>
    ];
}

class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.closeButton = React.createRef();
        this.closeMenu = this.closeMenu.bind(this);
    }

    closeMenu() {
        this.closeButton.current.click();
    }

    render() {
        const {categories, page, locale} = this.props;

        return <div id="menu">
            <div id="slide-ctrl">
                <label className="menu-btn" htmlFor="menu-tgl" ref={this.closeButton}>
                    <i className="fas fa-bars"></i>
                </label>
            </div>
            <nav>
                <input id="nav-0" className="nav-tgl" type="radio" name="nav" defaultChecked />
                {categorizedPages.map((category, index) =>
                    NavSection(category, index, categories, page, locale, this.closeMenu)
                )}
            </nav>
        </div>;
    }
}

function BottomNav({page, locale}) {
    const pageOrder = concat(
        ...categorizedPages.map(cat => cat.pages)
    );
    const pageIndex = pageOrder.indexOf(page);

    let leftLink = null;
    let rightLink = null;

    if (pageIndex > 0) {
        leftLink = <Link className='prev-btn' to={`/${locale}/${pageOrder[pageIndex - 1]}`}>
            <i className="fas fa-chevron-left"></i> Previous
        </Link>;
    }

    if (pageIndex + 1 < pageOrder.length) {
        rightLink = <Link className='next-btn' to={`/${locale}/${pageOrder[pageIndex + 1]}`}>
            Next <i className="fas fa-chevron-right"></i>
        </Link>;
    }

    return <div>
        {rightLink}
        {leftLink}
    </div>;
}

export default class App extends React.Component {
    state = {
        categories: null,
        locale: "en",
    }

    constructor() {
        super();

        this.loadCategories();
    }

    loadCategories() {
        content('./en/categories.yaml').then(categories => {
            this.setState({categories});
        });
    }

    render() {
        return <HashRouter>
            <div className="wrapper">
                <Route exact path='/' render={() => (
                    <Redirect to='/en/intro' />
                )}/>

                <input id="menu-tgl" type="checkbox"/>
                <label className="modal-background" htmlFor="menu-tgl"></label>

                <input id="lang-tgl" type="checkbox" />
                <label className="lang-modal-background" htmlFor="lang-tgl"></label>

                <header>
                    <div id="tr-box">
                        <label className="lang-btn" htmlFor="lang-tgl">
                            <i className="fas fa-globe"></i>
                        </label>
                    </div>
                    <Link id="logo" to="/">
                        <i className="fas fa-bolt"></i>
                        <span>Way To Go</span>
                    </Link>
                    <label className="menu-btn" htmlFor="menu-tgl">
                        <i className="fas fa-bars"></i>
                    </label>
                </header>

                <Route path="/:locale/:page" render={
                    props => <LanguageSelect
                        locale={props.match.params.locale}
                        page={props.match.params.page} />
                } />

                <Route path="/:locale/:page" render={
                    props => <TopLevelNav
                        categories={this.state.categories}
                        locale={props.match.params.locale}
                        page={props.match.params.page} />
                } />
                <Route path="/:locale/:page" render={
                    props => <Menu
                        categories={this.state.categories}
                        locale={props.match.params.locale}
                        page={props.match.params.page} />
                } />

                <div id="content">
                    <Route path="/:locale/:page" render={
                        props => <Lesson
                            locale={props.match.params.locale}
                            page={props.match.params.page} />
                    } />
                </div>

                <footer>
                    <Route path="/:locale/:page" render={
                        props => <BottomNav
                            locale={props.match.params.locale}
                            page={props.match.params.page} />
                    } />
                    <div className="clear"></div>
                </footer>
                <div className="sub-footer">
                    <a href="https://gitlab.com/way-to-go/way-to-go.gitlab.io"
                        className="project-link">
                        <i className="fab fa-gitlab"></i> way-to-go
                    </a>
                    &mdash; Powered by
                    <a href="https://duckpun.ch" className="personal-link">
                        duckpunch
                    </a>
                </div>

            </div>
        </HashRouter>;
    }
}
