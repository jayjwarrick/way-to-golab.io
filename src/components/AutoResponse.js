import React from 'react';
import godash from 'godash';
import {Goban} from 'react-go-board';
import {partial} from 'lodash';
import success from '../success.svg';
import fail from '../fail.svg';

export default class AutoResponse extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            board: this.initBoard(),
            tree: props.tree || {},
        };

        this.handleCoordinateClick = this.handleCoordinateClick.bind(this);
        this.handleNextTree = this.handleNextTree.bind(this);
        this.reset = this.reset.bind(this);
    }

    initBoard() {
        const {initWhite = [], initBlack = [], size = 19} = this.props;

        const blackBoard = godash.placeStones(
            new godash.Board(size),
            initBlack.map(godash.sgfPointToCoordinate),
            godash.BLACK,
        );

        return godash.placeStones(
            blackBoard,
            initWhite.map(godash.sgfPointToCoordinate),
            godash.WHITE,
        );
    }

    handleCoordinateClick(coordinate) {
        if (this.state.tree.result) {
            return;
        }

        this.setState({
            board: godash.addMove(this.state.board, coordinate, godash.BLACK),
        });

        const treeUpdate = partial(this.handleNextTree, coordinate);
        setTimeout(treeUpdate, 500);
    }

    handleNextTree(lastCoordinate) {
        const sgfPoint = godash.coordinateToSgfPoint(lastCoordinate);
        const tree = this.state.tree[sgfPoint] || this.state.tree.zz;

        const newState = {
            tree,
            result: tree.result,
        };

        if (tree.response) {
            const response = godash.sgfPointToCoordinate(tree.response);
            newState.board = godash.addMove(this.state.board, response, godash.WHITE);
        }

        this.setState(newState);
    }

    reset() {
        this.setState({
            board: this.initBoard(),
            tree: this.props.tree || {},
            result: null,
        });
    }

    render() {
        const {annotations = []} = this.props;
        const coordinateAnnotations = annotations.map(godash.sgfPointToCoordinate);

        let modal = null;

        if (this.state.result === 'success') {
            modal = <div className='board-modal' onClick={this.reset}>
                <img className='board-modal-button' src={success} alt='success' />
            </div>;
        } else if (this.state.result === 'fail') {
            modal = <div className='board-modal' onClick={this.reset}>
                <img className='board-modal-button' src={fail} alt='fail' />
            </div>;
        }

        return <div className='board'>
            <Goban board={this.state.board}
                boardColor='#eda'
                annotations={coordinateAnnotations}
                onCoordinateClick={this.handleCoordinateClick}/>
            {modal}
        </div>;
    }
}
