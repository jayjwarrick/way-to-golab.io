import AutoResponse from './AutoResponse';
import Freeplay from './Freeplay';
import Replay from './Replay';
import StaticBoard from './StaticBoard';
import CommonMark from 'commonmark';
import React from 'react';
import ReactRenderer from 'commonmark-react-renderer';
import content from '../content';
import {startsWith} from 'lodash';

export default class Lesson extends React.Component {
    state = {
        loadedPage: '',
        loadedLocale: '',
        pageContent: '',
        pagePositions: {},
    }

    async UNSAFE_componentWillMount() {
        this.loadContent(this.props);
    }

    async UNSAFE_componentWillReceiveProps(nextProps) {
        const {locale, page} = this.props;

        if (locale !== nextProps.locale || page !== nextProps.page) {
            this.loadContent(nextProps);
        }
    }

    async loadContent({locale, page}) {
        this.setState({
            loadedPage: page,
            loadedLocale: locale,
            pageContent: await content(`./${locale}/${page}.md`) || content(`./${locale}/404.md`),
            pagePositions: await content(`./positions/${page}.yaml`) || Promise.resolve({}),
        });
    }

    render() {
        const {pageContent, pagePositions} = this.state;

        const parser = new CommonMark.Parser();
        const renderer = new ReactRenderer();

        return <div className='lesson'>
            {renderer.render(parser.parse(pageContent)).map(
                (parsed, index) => {
                    const inner = parsed.props.children && parsed.props.children[0];
                    if (startsWith(inner, 'Board::')) {
                        const boardKey = inner.split('::')[1];
                        const boardProps = pagePositions[boardKey];
                        const listKey = `${this.state.loadedLocale}:${this.state.loadedPage}:${boardKey}`;

                        switch(boardProps.type) {
                            case 'auto-response':
                                return <AutoResponse key={listKey} {...boardProps}/>;
                            case 'freeplay':
                                return <Freeplay key={listKey} {...boardProps}/>;
                            case 'replay':
                                return <Replay key={listKey} {...boardProps}/>;
                            case 'static':
                                return <StaticBoard key={listKey} {...boardProps}/>;
                            default:
                                return null;
                        }
                    }
                    return parsed;
                }
            )}
        </div>;
    }
}
