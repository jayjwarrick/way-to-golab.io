import React from 'react';
import {Goban} from 'react-go-board';
import godash from 'godash';
import ControlBar from './ControlBar';

export default class Replay extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentMoveIndex: 0,
        };
    }

    handleReset() {
        this.setState({
            currentMoveIndex: 0,
        });
    }

    onPrev() {
        const newIndex = Math.max(0, this.state.currentMoveIndex - 1);
        this.setState({
            currentMoveIndex: newIndex,
        });
    }

    onNext() {
        const newIndex = Math.min(
            this.props.moves.length,
            this.state.currentMoveIndex + 1,
        );
        this.setState({
            currentMoveIndex: newIndex,
        });
    }

    render() {
        const moves = this.props.moves.slice(0, this.state.currentMoveIndex);
        const board = godash.constructBoard(
            moves.map(godash.sgfPointToCoordinate),
            new godash.Board(this.props.size),
        );

        return <div className='board'>
            <Goban boardColor='#eda' board={board} />
            <ControlBar onReset={this.handleReset.bind(this)}
                onNext={this.onNext.bind(this)} onPrev={this.onPrev.bind(this)}/>
        </div>;
    }
}
