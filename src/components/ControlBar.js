import React from 'react';
import next from '../next.svg';
import prev from '../prev.svg';
import reset from '../reset.svg';

export default function ControlBar({onReset, onNext, onPrev}) {
    return <div className='control-bar'>
        <div className='control-bar-card'>
            {onPrev && <img className='control-button' src={prev} alt='reset' onClick={onPrev}/>}
            {onNext && <img className='control-button' src={next} alt='reset' onClick={onNext}/>}
            <img className='control-button' src={reset} alt='reset' onClick={onReset}/>
        </div>
    </div>;
}
